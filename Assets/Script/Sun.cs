﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sun : MonoBehaviour {
    public float x = 0.0f;

	// Use this for initialization
	void Start () {
        gameObject.transform.Rotate(90,0,0);
	}
	
	// Update is called once per frame
	void Update () {
        if (gameObject.transform.rotation.x >= 180.0f)
        {
            gameObject.transform.rotation = Quaternion.Euler(0, 0, 0);
        }
        gameObject.transform.Rotate(gameObject.transform.rotation.x*Time.smoothDeltaTime*2.0f, 0, 0);
	}
}
