﻿    using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallController : MonoBehaviour {
    public Material[] material;
    public AudioClip Boaw;
    public ParticleSystem Cloud;
	Renderer rend;
	// Use this for initialization
	void Start () {
        GetComponent<AudioSource>().playOnAwake = false;
        GetComponent<AudioSource>().clip = Boaw;
        Renderer[] blocks = GetComponentsInChildren<Renderer> ();
		rend = GetComponent<Renderer>();
		rend.enabled = true;
		int last = 0;
		int rng;
		foreach (var x in blocks) {
            
			do {
				rng = Random.Range (0, material.Length-1);
			} while (rng == last);
            last = rng;
            x.enabled = true;
			x.sharedMaterial =material[rng];
		}
	}
	
	// Update is called once per frame
	/*void collision (Collision collide) 
	{
		if (collide.gameObject.tag == "Ball") 
		{
			rend.sharedMaterial= material[1];
		} 
		else 
		{
			rend.sharedMaterial= material[2];
		}
	}*/
}
