using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour 
{
	public float turnspeed = 8f;
	public Rigidbody rgBody;
    public GameObject ball;
    public float velocity;
    public bool launched = false;
    public GameObject shield;
    // Use this for initialization
    void Start () {
        shield.SetActive(false);
		rgBody = GetComponent<Rigidbody> ();
		transform.position = new Vector3 (0,1.5f,0);
	}
	
	// Update is called once per frame
	void Update () {
		var strafe = turnspeed;
		if (Input.GetKey (KeyCode.D)) {
			transform.Rotate (0, strafe*Time.deltaTime, 0);
		}
		else if(Input.GetKey (KeyCode.A)) {
			transform.Rotate (0, -strafe*Time.deltaTime, 0);
		}
        if (launched == false)
        {
            if (Input.GetKey(KeyCode.Space))
            {
                launched = true;
                ball.transform.parent = null;
                ball.GetComponent<Rigidbody>().AddForce(GameObject.Find("Main Camera").transform.forward * 100);
            }
        }
        if (Input.GetMouseButtonDown(1))
        {
            shield.SetActive(true);
        }
	}
}