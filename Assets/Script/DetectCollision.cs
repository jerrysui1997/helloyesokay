﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectCollision : MonoBehaviour {
    public AudioClip boop;
    public Rigidbody rigBody;
    public GameObject Shield;
    // Use this for initialization
    void Start () {
        GetComponent<AudioSource>().playOnAwake = false;
        GetComponent<AudioSource>().clip = boop;
        transform.position.Set(transform.position.x, transform.position.y, transform.position.z + 3);
    }
	
	// Update is called once per frame
	void Update () {
    }
    private void OnCollisionExit(Collision collision)
    {
        Vector3 relativePos = GameObject.Find("RealPlayer").transform.position - gameObject.transform.position;
        if (collision.relativeVelocity.magnitude <1)
        {
            gameObject.GetComponent<Rigidbody>().AddForce(10 * relativePos);
        }
    }
    void OnCollisionEnter(Collision collision)
    {
        Debug.Log("collided!");
        Debug.Log("collided with" + collision.gameObject.name);
        if (collision.gameObject.tag == "Box")
        {
            Vector3 relativePos = GameObject.Find("RealPlayer").transform.position - gameObject.transform.position;
            Debug.Log(collision.relativeVelocity.magnitude);
            relativePos.y += 0.7f;
            if(collision.relativeVelocity.magnitude <20)
            {
                gameObject.GetComponent<Rigidbody>().AddForce(20 * relativePos);
            }
            Debug.Log(collision.gameObject.GetComponent<Renderer>().material + "    " + GetComponent<Renderer>().material);
            if (collision.gameObject.GetComponent<Renderer>().sharedMaterial.name == GetComponent<Renderer>().sharedMaterial.name)
            {
                Debug.Log("same material!");
                GetComponent<AudioSource>().Play();
                Destroy(collision.gameObject);
                WallController x = collision.gameObject.GetComponentInParent<WallController>();
                x.Cloud.transform.position = collision.gameObject.transform.position;
                x.Cloud.Play();
            }
            else
            {
                collision.gameObject.GetComponentInParent<AudioSource>().Play();
            }
        }
    }
}
