﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeneralCollision : MonoBehaviour
{
    private void OnCollisionEnter(Collision collision)
    {
        Rigidbody colrigid = collision.gameObject.GetComponent<Rigidbody>();
        Debug.Log("collision detected with: " + collision.gameObject.name + " and " + gameObject.name);
        Ray ballray = new Ray(collision.transform.position, collision.relativeVelocity);
        RaycastHit hit;

        Physics.Raycast(ballray, out hit, colrigid.velocity.magnitude + 1f);
        Vector3 Reflection = Vector3.Reflect(ballray.direction, hit.normal);
        if (collision.relativeVelocity.magnitude < 15)
        {
            colrigid.AddForce(7 * Reflection);
            Debug.Log(collision.relativeVelocity.magnitude);
        }

    }
}