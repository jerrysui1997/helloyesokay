﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldCollision : MonoBehaviour {


    // Update is called once per frame
    private void OnCollisionEnter(Collision collision)
    {
        Debug.Log("Shield collision detected");
        Rigidbody colrigid = collision.gameObject.GetComponent<Rigidbody>();
        Ray ballray = new Ray(collision.transform.position,collision.transform.forward);
        RaycastHit hit;

        Physics.Raycast(ballray, out hit, colrigid.velocity.magnitude + 1f);
        Vector3 Reflection = Vector3.Reflect(ballray.direction, hit.normal);
        float rot = 90 - Mathf.Atan2(Reflection.z, Reflection.y) * Mathf.Rad2Deg;
        if (collision.relativeVelocity.magnitude < 20)
        {
            collision.gameObject.transform.Rotate(0, rot, 0);
            colrigid.AddForce(7 * collision.transform.forward);
        }
    }
}

